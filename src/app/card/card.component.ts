import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})

export class CardComponent implements OnInit {
  public TaskList = [''];
  public taskName = '';
  public select = 0;
  public EmptyList = false;
  public AddList = false;
  public EditList = false;
  public DeleteList = false;
  public ClickInputText = false;
  public message = '';
  public showButton = false;
  public nameButton = '';

  constructor()
  {
    this.TaskList = [];
    this.EmptyList = false;
    this.AddList = false;
    this.EditList = false;
    this.DeleteList = false;
    this.ClickInputText = false;
    this.showButton = false;
    this.nameButton = 'Submit!';
  }

  ngOnInit(): void
  {

  }

  public AddItem()
  {
    if(this.taskName.trim().length <=0){
      this.EmptyList = false;
      this.AddList = false;
      this.EditList = false;
      this.DeleteList = false;
      this.ClickInputText = true;
      this.message = 'Please Enter Value';
    }
    else if(this.select == 0){
      let task = this.taskName;
      this.TaskList.push(task);
      this.taskName = '';

      this.EmptyList = false;
      this.AddList = true;
      this.EditList = false;
      this.DeleteList = false;
      this.ClickInputText = false;
      this.message = 'Add Item Sucess';
    }
    else if(this.select >= 1){
      let task = this.taskName;
      this.TaskList[this.select -1] = this.taskName;
      this.taskName = '';

      this.EmptyList = false;
      this.AddList = false;
      this.EditList = true;
      this.DeleteList = false;
      this.ClickInputText = false;
      this.message = 'Value change Sucess';
      this.select = 0;
      this.nameButton = 'Submit!'
    }
    else{

    }

    this.ShowHideButtonDelete();
  }

  public EditItem(index: number)
  {
    this.select = index + 1;
    this.taskName = this.TaskList[index];
    this.nameButton = 'Edit';
  }

  public DeleteItem(index: number)
  {
    this.TaskList.splice(index, 1);

    this.EmptyList = false;
    this.AddList = false;
    this.EditList = false;
    this.DeleteList = true;
    this.ClickInputText = false;
    this.message = 'Delete Item Sucess';

    this.ShowHideButtonDelete();
  }

  public ClearItem()
  {
    this.TaskList = [];
    this.EmptyList = true;
    this.AddList = false;
    this.EditList = false;
    this.DeleteList = false;
    this.ClickInputText = false;

    this.message = 'Empty List';
    this.ShowHideButtonDelete();
  }

  public ShowHideButtonDelete()
  {
    if(this.TaskList.length <1){
      this.showButton = false;
    }
    else{
      this.showButton = true;
    }
  }

  public hideMessage()
  {
    this.EmptyList = false;
    this.AddList = false;
    this.EditList = false;
    this.DeleteList = false;
    this.ClickInputText = true;
    this.message = 'Please Enter Value';
  }
}

